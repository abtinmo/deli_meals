import 'package:flutter/material.dart';
import '../dummy_data.dart';
import '../widgets/meal_item.dart';

class CategeoriyMealsScreen extends StatelessWidget {
  static const routeName = '/category-meals';
//   final String title;
//   final String id;

// CageoriyMealsScreen(this.title, this.id);

  @override
  Widget build(BuildContext context) {
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    final filterdMeals = DUMMY_MEALS.where((meal) {
      return meal.categories.contains(routeArgs["id"]);
    }).toList();
    return Scaffold(
        appBar: AppBar(title: Text(routeArgs['title'])),
        body: ListView.builder(
          itemBuilder: (ctx, index) {
            return MealItem(filterdMeals[index]);
          },
          itemCount: filterdMeals.length,
        ));
  }
}
